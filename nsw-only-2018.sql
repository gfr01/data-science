select st[0] as start_x, st[1] as start_y, en[0] as end_x, en[1] as end_y, 
						  * from (select 
  -- (select location from tripdetail td1 where td1.tripid = t.id and "index"=(select min("index") from tripdetail td2 where td2.tripid = t.id and location is not null) and distance < 1 limit 1) as trip_start_loc
  --,(select location from tripdetail td1 where td1.tripid = t.id and "index"=(select max("index") from tripdetail td2 where td2.tripid = t.id and location is not null) limit 1) as trip_end_loc
   (select geopoint from "location" where starttripid = t.id limit 1) as st
  ,(select geopoint from "location" where endtripid = t.id limit 1) as en
  , *
  from trip t 
  where EXTRACT(YEAR FROM "endtime")::INT = 2018
  order by endtime
  limit 100000 offset 0
  ) as ret
	
	--, ST_MakePoint(trip_start_loc) as "start"
  where 
	ret."st" is not null and ret."en" is not null
	and (ret."st"::point @ box '((141, -29), (155, -37))')
  --limit 100
  ;

