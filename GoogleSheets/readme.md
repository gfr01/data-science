You need to create DB_Credentials.py in Credentials folder that should be created when you pull this branch from bitbucket to run this script

Create a dictionary as follows in DB_Credentials.py
Productionlogin = {	'username' : 'Your Username',
					'password' : 'Your Password' }
					

To setup environment for connecting and sending data to Google Sheets follow the instruction on website
https://developers.google.com/sheets/api/quickstart/python

To install other dependencies You need to 
pip install -r Requirements.txt You obviously need to have pip installed already on your system