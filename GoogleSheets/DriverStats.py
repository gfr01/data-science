
from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from slacker import Slacker
import psycopg2
from tabulate import tabulate

# Read readme.md for information regarding DB_Credentials

import sys
sys.path.append('..')
from Credentials import DB_Credentials

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Send Driver Stats to Google Sheet'


def get_credentials():

    home_dir = os.path.expanduser('..')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def main():

    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    spreadsheetId = '1d_KuDowryj-xpcFeu5gqY1Zn00zi8rn8ClfscXqf03s'


    ### Connecting TO Database #########################
    DB_user_name = DB_Credentials.Productionlogin['username']
    DB_password  = DB_Credentials.Productionlogin['password']

    SIRA_USER_NAME = DB_Credentials.Siralogin['username']
    SIRA_password  = DB_Credentials.Siralogin['password']
    


    try:
        conn = psycopg2.connect(database="gofar", user=DB_user_name, password=DB_password,
                                host="gofar.cio97bf7lbrv.ap-southeast-2.rds.amazonaws.com")
        print ("Connected to Production database\n")
    except:
        print ("Can't connect to Production database\n")
        return 1
    
    '''if 0: \
    try:
        connSIRA = psycopg2.connect(database="sira", user=SIRA_USER_NAME, password=SIRA_password,
                                host="gofar-sira.cio97bf7lbrv.ap-southeast-2.rds.amazonaws.com")
        print ("Connected to Sira database\n")
    except:
        print ("Can't connect to SIRA database\n")
        return 1
    '''


    cur = conn.cursor()
    #siraCur = connSIRA.cursor()
    ####################################################

    ############# Slack Update ########################
    slack = Slacker('xoxp-2598177605-12987404054-79924332247-1548e4b017')

    # Fetch Distance travelled
    quer = "select sum(distance) from trip ;"
    cur.execute(quer)
    result = cur.fetchall()
    dist = result[0]
    
    #siraCur.execute(quer)
    #result = siraCur.fetchall()
    distSira = result[0]

    Hello = " Hello fellow GOFAR guys! The driver stats are as follows:  \n"

    # Fetch total drivers on Production

    quer = "select count(distinct deviceserialnumber) from trip where startTime > '2016-03-15'"
    cur.execute(quer)
    result = cur.fetchall()
    NumUsers = result[0]

    #siraCur.execute(quer)
    #result = siraCur.fetchall()
    #NumUsersSira = result[0]

    # Fetch DAU on Production

    quer = "select count(distinct trip.deviceserialnumber) from trip where trip.starttime between (current_date - interval '2 day') AND (current_date - interval '1 day')"
    cur.execute(quer)
    result = cur.fetchall()
    DAU = result[0]

    #siraCur.execute(quer)
    #result = siraCur.fetchall()
    #DAUSira = result[0]
    
    # Fetch 7 days rolling Active Drivers on Production

    quer = "select count(distinct trip.deviceserialnumber) from trip where trip.starttime between (current_date - interval '8 day') AND (current_date - interval '1 day')"
    cur.execute(quer)
    result = cur.fetchall()
    RollWAU = result[0]

    #siraCur.execute(quer)
    #result = siraCur.fetchall()
    #RollWAUSira = result[0]
    
    # Fetch new DTC in last 24 hours

    quer = (" SELECT count(*) from diagnostictroublecode inner join vehicle on diagnostictroublecode.vehicleid = vehicle.id "
            " inner join \"user\" on \"user\".id = vehicle.userid::uuid where \"user\".email NOT LIKE '%@gofar.co' "
            )

    cur.execute(quer)
    result = cur.fetchall()

    NewDtc = result[0]

    quer = ( "select count(distinct trip.deviceserialnumber) from trip where trip.starttime between (current_date - interval '8 day') AND (current_date - interval '1 day')"
            " AND trip.endtimezone = 'Australia/Sydney';")
    cur.execute(quer)
    result = cur.fetchall()

    RollWAUSydney = result[0]

    slackMessage = tabulate([['Total Distance in Km', int(dist[0])],
                             ['Total Drivers on Production', int(NumUsers[0])],
                             ['Daily Active Drivers', int(DAU[0])],
                             ['7 Days Rolling Active Drivers Global', int(RollWAU[0])],
                             ['7 Day Rolling Active Drivers in Sydney Timezone', int(RollWAUSydney[0])],
                             ['Total DTCs', int(NewDtc[0])]], headers=['Metric', 'Quantity'])
    
    '''siraMessage = tabulate([['SIRA Total Distance [Km]', int(distSira[0])],
                             ['SIRA Total Drivers', int(NumUsersSira[0])],
                             ['SIRA Daily Active Drivers', int(DAUSira[0])],
                             ['SIRA 7 Days Rolling Active Drivers', int(RollWAUSira[0])]])
    '''
    print(slackMessage)
    #connSIRA.close()                       
    slack.chat.post_message('#general', Hello)
    slack.chat.post_message('#general', slackMessage)
    #slack.chat.post_message('#general', siraMessage)                          
                           
    ##################################################

    #############Updating DAU###########################

    quer = "select to_char(date(date_trunc('day',trip.starttime)),'DD Mon YYYY'), count(distinct trip.deviceserialnumber) from trip where trip.starttime > '2016-03-15' AND trip.starttime < (current_date - interval '1 day') group by date(date_trunc('day',trip.starttime));"
    cur.execute(quer)

    result = cur.fetchall()
    rangeName = 'DAU!A2:B'

    data = {
        'range': rangeName,
        'values': result,
        'majorDimension': 'ROWS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()

    #####################################################

    #############Updating WAU###########################

    quer = "select to_char(date(date_trunc('week',trip.starttime)),'DD Mon YYYY'), count(distinct trip.deviceserialnumber) from trip where trip.starttime > '2016-03-15' AND trip.starttime < (current_date - interval '1 day') group by date(date_trunc('week',trip.starttime));"
    cur.execute(quer)

    result = cur.fetchall()
    wauCol = []
    for x in result:
        wauCol.append(int(x[1]))


    rangeName = 'WAU!A2:B'

    data = {
        'range': rangeName,
        'values': result,
        'majorDimension': 'ROWS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()

    #####################################################



    #############Updating Total Active Driver###########################

    quer = ("select to_char(date(date_trunc('week',\"user\".createdat)),'DD Mon YYYY'), sum(count(distinct(\"user\".email))) OVER (ORDER BY date_trunc('week',\"user\".createdat)) "
            " from \"user\" inner join vehicle on \"user\".id = vehicle.userid inner join trip on vehicle.id = trip.vehicleid "
            " where \"user\".email NOT LIKE '%@test.com' AND \"user\".createdat > '2016-03-15' AND \"user\".createdat < (current_date - interval '1 day')  AND trip.distance > 0 "
            " group by date_trunc('week',\"user\".createdat); ")

    cur.execute(quer)

    result = cur.fetchall()



    dateCol = []
    userCol = []
    wauRatioCol = []

    for x in result:
        dateCol.append(x[0])
        userCol.append(int(x[1]))

    for i in range(0,len(userCol)):
        wauRatioCol.append(int(float(wauCol[i])/float(userCol[i])*100))

    rangeName = 'WAU!C2:D'

    data = {
        'range': rangeName,
        'values': [userCol,wauRatioCol],
        'majorDimension': 'COLUMNS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()



    #####################################################


    #############Updating MAU###########################

    quer = "select to_char(date(date_trunc('month',trip.starttime)),'MONTH YYYY'), count(distinct trip.deviceserialnumber) from trip where trip.starttime > '2016-03-15' AND trip.starttime < (current_date - interval '0 day') group by date(date_trunc('month',trip.starttime));"
    cur.execute(quer)

    result = cur.fetchall()


    rangeName = 'MAU!A2:B'

    data = {
        'range': rangeName,
        'values': result,
        'majorDimension': 'ROWS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
            spreadsheetId=spreadsheetId, body=rbody).execute()

    #######################################################################


    #############Updating LAG #############################################

    ############ Fetching number of trips since 1st Arpil 2016 that synced within hr, under 24, under 72 hr and so on till over 168 hr ie a week ########
    quer = (" select to_char(date(date_trunc('day',trip.createdat)),'DD Mon YYYY') as DATE,"
            " count(case when EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) < 10000 THEN 1 END) as alltrips,"
            " count(case when EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) < 1 THEN 1 END) as underhr,"
            " count(case when EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) >= 1 "
            " AND EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60)  < 24  THEN 1 END) as under24hr,"
            " count(case when EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) >= 24 "
            " AND EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) < 72  THEN 1 END) as over24hr,"
            " count(case when EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) >= 72 "
            " AND EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) < 168  THEN 1 END) as over72hr,"
            " count(case when EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) >= 168 "
            " AND EXTRACT(EPOCH FROM age(trip.createdat,trip.endtime)::INTERVAL/60/60) < 10000  THEN 1 END) as over168hr"
            " from trip WHERE (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%')"
            " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2016-04-01' "
            "AND trip.updatedat < (current_date - interval '2 day') GROUP BY date_trunc('day',trip.createdat);")

    cur.execute(quer)
    result = cur.fetchall()

    DateUpdateTrips = []
    PercentUnderHr = []
    PercentUnder24Hr = []
    PercentOver24Hr = []
    PercentOver72Hr = []
    PercentOver168Hr = []

    for x in result:
        DateUpdateTrips.append(x[0])
        PercentUnderHr.append(round(float(x[2])/float(x[1])*100,2))
        PercentUnder24Hr.append(round(float(x[3])/float(x[1])*100,2))
        PercentOver24Hr.append(round(float(x[4])/float(x[1])*100,2))
        PercentOver72Hr.append(round(float(x[5])/float(x[1])*100,2))
        PercentOver168Hr.append(round(float(x[6])/float(x[1])*100,2))

    rangeName = 'Lag!A2:G'

    data = {
        'range': rangeName,
        'values': [DateUpdateTrips, PercentUnderHr,PercentUnder24Hr,PercentOver24Hr,PercentOver72Hr,PercentOver168Hr],
        'majorDimension': 'COLUMNS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()

    #####################################################


    ########### Updating Offline/Online trip % #########
    quer = (" select to_char(date(date_trunc('day',trip.updatedat)),'DD Mon YYYY'),count(*) from trip where (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%')"
            " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2016-04-01' AND trip.updatedat < (current_date - interval '2 day')  AND trip.starttimezone IS NOT NULL group by date_trunc('day',trip.updatedat) "
            " ORDER BY date_trunc('day',trip.updatedat) ;")  # Online Trips
    cur.execute(quer)

    result = cur.fetchall()

    DateCol = []
    NumTripsOnline = []

    for x in result:
        DateCol.append(x[0])
        NumTripsOnline.append(x[1])

    quer = (" select to_char(date(date_trunc('day',trip.updatedat)),'DD Mon YYYY'),count(*) from trip where (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%') "
            " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2016-04-01' AND trip.updatedat < (current_date - interval '2 day')  GROUP BY date_trunc('day',trip.updatedat) ORDER BY date_trunc('day',trip.updatedat) ;")  # All the trips
    cur.execute(quer)

    result = cur.fetchall()

    NumTrips = []
    for x in result:
        NumTrips.append(x[1])

    OnlinePrct = []
    for i in range(0,len(NumTrips)):
        OnlinePrct.append(round(float(NumTripsOnline[i])/float(NumTrips[i])*100,2))


    rangeName = 'Lag!I2:J'

    data = {
        'range': rangeName,
        'values': [DateCol ,OnlinePrct],
        'majorDimension': 'COLUMNS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()
    ####################################################

    ########### Updating Offline/Online trip for iOS % #########
    quer = (
    " select to_char(date(date_trunc('day',trip.updatedat)),'DD Mon YYYY'),count(*) from trip where (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%')"
    " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2017-07-01' AND trip.updatedat < (current_date - interval '2 day') "
    " AND trip.starttimezone IS NOT NULL "
    " AND trip.mobileappversion LIKE 'iOS%' "
    " group by date_trunc('day',trip.updatedat) "
    " ORDER BY date_trunc('day',trip.updatedat) ;")  # Online Trips
    cur.execute(quer)

    result = cur.fetchall()

    DateColIOS = []
    NumTripsOnlineIOS = []

    for x in result:
        DateColIOS.append(x[0])
        NumTripsOnlineIOS.append(x[1])

    quer = (
    " select to_char(date(date_trunc('day',trip.updatedat)),'DD Mon YYYY'),count(*) from trip where (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%')"
    " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2017-07-01' AND trip.updatedat < (current_date - interval '2 day') "
    " AND trip.mobileappversion LIKE 'iOS%' "
    " group by date_trunc('day',trip.updatedat) "
    " ORDER BY date_trunc('day',trip.updatedat) ;") # All the trips
    cur.execute(quer)

    result = cur.fetchall()

    NumTripsIOS = []
    for x in result:
        NumTripsIOS.append(x[1])

    OnlinePrct = []
    for i in range(0, len(NumTripsIOS)):
        OnlinePrct.append(round(float(NumTripsOnlineIOS[i]) / float(NumTripsIOS[i]) * 100, 2))

    rangeName = 'Lag!L2:M'

    data = {
        'range': rangeName,
        'values': [DateColIOS, OnlinePrct],
        'majorDimension': 'COLUMNS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()
    ####################################################


    ########### Updating Offline/Online trip for Android % #########
    quer = (
    " select to_char(date(date_trunc('day',trip.updatedat)),'DD Mon YYYY'),count(*) from trip where (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%')"
    " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2017-07-01' AND trip.updatedat < (current_date - interval '2 day') "
    " AND trip.starttimezone IS NOT NULL "
    " AND trip.mobileappversion LIKE 'Android%' "
    " group by date_trunc('day',trip.updatedat) "
    " ORDER BY date_trunc('day',trip.updatedat) ;")  # Online Trips
    cur.execute(quer)

    result = cur.fetchall()

    DateColAndroid = []
    NumTripsOnlineAndroid = []

    for x in result:
        DateColAndroid.append(x[0])
        NumTripsOnlineAndroid.append(x[1])

    quer = (
    " select to_char(date(date_trunc('day',trip.updatedat)),'DD Mon YYYY'),count(*) from trip where (trip.deviceserialnumber LIKE 'DMX1526%' OR trip.deviceserialnumber LIKE 'GFR%')"
    " AND trip.firmwareversion > '4.0.0.0' AND trip.updatedat > '2017-07-01' AND trip.updatedat < (current_date - interval '2 day') "
    " AND trip.mobileappversion LIKE 'Android%' "
    " group by date_trunc('day',trip.updatedat) "
    " ORDER BY date_trunc('day',trip.updatedat) ;") # All the trips
    cur.execute(quer)

    result = cur.fetchall()

    NumTripsAndroid = []
    for x in result:
        NumTripsAndroid.append(x[1])

    OnlinePrct = []
    for i in range(0, len(NumTripsAndroid)):
        OnlinePrct.append(round(float(NumTripsOnlineAndroid[i]) / float(NumTripsAndroid[i]) * 100, 2))

    rangeName = 'Lag!O2:P'

    data = {
        'range': rangeName,
        'values': [DateColAndroid, OnlinePrct],
        'majorDimension': 'COLUMNS'
    }
    rbody = {
        'valueInputOption': 'USER_ENTERED',
        'data': data
    }

    result = service.spreadsheets().values().batchUpdate(
        spreadsheetId=spreadsheetId, body=rbody).execute()
    ####################################################



    conn.close()

if __name__ == '__main__':
    main()
