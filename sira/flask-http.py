#!/usr/bin/python
import sys

sys.dont_write_bytecode = True

from flask import Flask, request, send_from_directory, after_this_request

import os
from sira_stats import main as generate_csv

# AUTH START

from functools import wraps
from flask import request, Response
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'admin' and password == 'sirapassword'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

# AUTH END

userDict = None

def anon_email2siraid(email, userDict = userDict):
    import csv
    if not userDict:
        with open('user_list.csv', mode='r') as infile:
            reader = csv.reader(infile)
            userDict = {rows[2].lower():rows[1] for rows in reader}
    return email in userDict and userDict[email] or None

"""
$ python -i
Python 2.7.12 (default, Nov 12 2018, 14:36:49) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> (1, 3, 2, 3,) >= (2, )
False
>>> (1, 3, 2, 3,) >= (2, 0, 0, 0,)
False
>>> (1, 3, 2, 3,) >= (1, 0, 0, 0,)
True
>>> (1, 3, 2, 3,) >= (1, 0, 0, 0,)
"""

def is_ver_supported(ver = (1, 0, 0, 8, ), src = 'Android'):
    if ',' in src:
        src = src.split(',')[0]
    if ',' in ver:
        ver = ver.split(' ')[1]
    if isinstance(ver, basestring):
        #print 'ver', ver
        ver = tuple(map(int, ver.split('.')))
        #print ver, src
    if 'Android' in src:
        return ver >= (1, 0, 5, 12, )
    else:
        #print 'ios'
        #print ver
        return ver >= (1, 1, 5, )

assert is_ver_supported((1, 0, 6, 12), "Android") == True, "1.0.6.12 And should be supported"
assert is_ver_supported((1, 0, 5, 10), "Android") == False, "1.0.5.10 And should be not supported"
assert is_ver_supported((1, 0, 6, 12), "Android") == True, "1.0.6.12 And should be supported"
assert is_ver_supported((1, 1, 5, ), "iOS") == True, "1.1.5 should be supported"
assert is_ver_supported((1, 1, 4, ), "iOS") == False, "1.1.4 should not be supported"
assert is_ver_supported((1, 0, 4, ), "iOS") == False, "1.0.4 should not be supported"
assert is_ver_supported((1, 1, 5, ), "iOS") == True, "1.1.5 should be supported"
assert is_ver_supported('iOS, 1.1.5', "iOS") == True, "1.1.5 should be supported"

def anonymise_csv(filename, anon_file = 'user_list.csv'):
    import csv
    header, content = None, None
    with open(filename, mode='r') as infile:
        reader = csv.reader(infile)
        header = reader.next()
        content = [row for row in reader]
    with open(filename, mode='w') as outfile:
        writer = csv.writer(outfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(header)
        #content = [row for row in content]
        for i in xrange(len(content)):
            mob_app_ver = content[i][7]
            if mob_app_ver and not is_ver_supported(mob_app_ver):
                content[i] = content[i] + ["too_old_version"]
            email = anon_email2siraid(content[i][0])
            if email:
                content[i][0] = email
                # print(content[i])
                # print(content)
                writer.writerow(content[i])
            else:
                print "no email: ", content[i][0]
        # [writer.writerow(row) for row in content]

app = None

sys.path.append('./dataexport')

def _speeding_report(start = None, end = None, request = None, password = "SIRA_data", zipit = False):
    from query_postgres import executeSQL
    e_from, e_to = start or request.form['from'], end or request.form['to']
    import re
    if re.match(r"^(\d+-?)*$", e_from) and re.match(r"^(\d+-?)*$", e_to):
        filename = 'SIRA_SPEEDING_NOTIFICATIONS_' + e_from + 'SOD_' + e_to + 'EOD.csv'
        import os.path
        zipfile = filename + '.zip'
        #@after_this_request
        def remove_file(response):
            try:
                os.remove(filename)
                # file_handle.close()
            except Exception as error:
                app.logger.error("Error removing or closing downloaded file handle", error)
            return response

        if os.path.isfile(filename) and request:
            # return app.send_static_file(zipfile)
            return send_from_directory('.', filename, as_attachment=True)
        query = """
            SELECT
                -- tripdetail.*,
                REPLACE("user".email, '_turnedoff', '') as "email",
                trip.starttime,
                trip.endtime,
                tripdetail.index,
                subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT' AS "is_treatment",
                timestamp,
                speed,
                speedlimit
            FROM tripdetail
            left join trip on tripdetail.tripid = trip.id
            left join usertrip on usertrip.tripid = trip.id
            LEFT JOIN subscription ON subscription.userid = usertrip.userid
            LEFT JOIN "user" ON "user".id = subscription.userid
            WHERE usertrip.userid in
                (
                SELECT subscription.userid FROM subscription
                LEFT JOIN "user" ON "user".id = subscription.userid
                WHERE 
                    "user".email not like '%gofar.co%'
                    AND "user".email not like '%.pl'
                ) """ + """
                AND tripdetail.tripid in (
                    SELECT "id" FROM trip
                    WHERE trip.endtime >= DATE '%s' - INTERVAL '1 days'
                    AND trip.endtime <= DATE '%s 23:59:59'
                )
                and tripdetail.createdat >= '%s'
                and tripdetail.createdat <= '%s 23:59:59'
                AND "speed" > 0 and "speedlimit" >= 40 and "speedlimit" < 255
                ORDER BY tripdetail.createdat
            """ % (e_from, e_to, e_from, e_to, )
        query = """
            SELECT * FROM (
            SELECT
                REPLACE("user".email, '_turnedoff', '') as "email",
                trip.id,
                trip.starttime,
                trip.endtime,
                subscription.subscriptiontypeid = 'SIRA_USER_DEFAULT' AS "is_treatment",
                (
                    SELECT count(id)
                    FROM tripdetail td
                    WHERE
                        tripid = trip.id
                        AND "speed" > speedlimit AND "speedlimit" >= 40 AND "speedlimit" < 255
                ) AS speedingRowsCount, ( -- speeding more than 20% of the trip
                    SELECT count(id)
                    FROM tripdetail td
                    WHERE
                        tripid = trip.id
                        AND "speed" > 0 AND "speedlimit" >= 40 AND "speedlimit" < 255
                ) AS "non-idle-row-count",
                mobileappversion
            FROM trip
            LEFT JOIN usertrip on usertrip.tripid = trip.id
            LEFT JOIN "user" ON "user".id = usertrip.userid
            LEFT JOIN subscription ON subscription.userid = usertrip.userid
            WHERE trip.starttime >= '{} 00:00:00+11'
                AND trip.endtime >= timestamp '{} 00:00:00+11' - INTERVAL '1 days'
                AND trip.endtime <= '{} 23:59:59+11'
            ) AS tdetails
            WHERE speedingRowsCount > 0.2 * "non-idle-row-count"
            ORDER BY endtime
        """.format(e_from, e_from, e_to, )
        print 'query', query
        data = executeSQL(query, filename)
        print 'anonymisation', filename
        anonymise_csv(filename)
        print 'anonymisation done', filename
        import os
        if zipit: \
        os.system('zip -P "%s" -rm "%s" %s' % (
                password,
                zipfile,
                filename,
            )
        )
        print('finished zipping')
        return app and send_from_directory('.', filename, as_attachment=True)
        # return app.send_static_file(zipfile)
    return "Error."

if 0 and not 'ZIP_PASS' in os.environ:
    since = len(sys.argv) >= 2 and sys.argv[2]
    until = len(sys.argv) >= 2 and sys.argv[3]

    if len(sys.argv) > 1 and sys.argv[1] == 'spdrep' and since and until:
        _speeding_report(since, until)
    exit()

# set the project root directory as the static folder, you can set others.
if 1:
    pass
#if 'ZIP_PASS' in os.environ:

    app = Flask(__name__, static_url_path='', static_folder='')

@app.route('/csv/<path:path>')
@requires_auth
def send_js(path):
    return send_from_directory('csv', path)

@app.route('/')
@requires_auth
def root():
    return app.send_static_file('./index.html')

@app.route('/summary', methods=['GET', 'POST'])
@requires_auth
def summary_route():
    filename = generate_csv(create_zip = True, password = 'ZIP_PASS' in os.environ and os.environ['ZIP_PASS'])
    return send_from_directory('.', filename, as_attachment=True)
    # return app.send_static_file(filename, as_attachment=True)

sys.path.append('../GoogleSheets')
from DriverStats import main as slack # just for dev/veryfing installed dependencies/fail-fast approach

@app.route('/slack', methods=['GET', 'POST'])
@requires_auth
def summary_slack():
    # sys.path.append('../GoogleSheets')

    from DriverStats import main as slack
    return slack()

@app.route('/export', methods=['GET', 'POST'])
@requires_auth
def summary_trips():
    # sys.path.append('./dataexport')

    from DriverStats import main as slack
    return slack()

@app.route('/dataexport', methods=['GET', 'POST'])
@requires_auth
def data_export():
    # sys.path.append('./dataexport')

    from sira_export_c import main as sira_export_control_group
    print('req', request.form)
    e_from, e_to = request.form['from'], request.form['to']
    import re
    if re.match(r"^(\d+-?)*$", e_from) and re.match(r"^(\d+-?)*$", e_to):
        return sira_export_control_group(e_from, e_to, create_zip = True)

@app.route('/<path:path>')
@requires_auth
def static_file(path):
    return app.send_static_file(path)

@app.route('/db_iops')
@requires_auth
def aws_iops():
    import boto3
    import os
    client = boto3.client(
        's3',
        aws_access_key_id=os.environ['ACCESS_KEY'],
        aws_secret_access_key=os.environ['SECRET_KEY'],
        aws_session_token=os.environ['SESSION_TOKEN'],
    )
    metric = None
    image = metric.get_metric_widget_image()

@app.route('/vps_stats')
@requires_auth
def vps_stats():
    return {
        'freeSpace': os.system("df -h | grep da1 | awk '{print $4;}'"),
        'queries': """
            SELECT
                pid,
                now() - pg_stat_activity.query_start AS duration,
                query,
                state
                FROM pg_stat_activity
                WHERE (now() - pg_stat_activity.query_start) > interval '5 minutes';
            """
    }

@app.route('/speedingreport', methods=['GET', 'POST'])
@requires_auth
def speeding_report(start = None, end = None):
    return _speeding_report(start, end, request)

if __name__ == "__main__":
    os.environ['ZIP_PASS']
    print('urls', app.url_map)
    debug = 'ZIP_PASS' not in os.environ
    kwargs = {}
    if debug:
        kwargs['host'] = '0.0.0.0'
    app.run(debug=debug, **kwargs)
