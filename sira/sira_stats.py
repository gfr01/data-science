

# Read readme.md for information regarding DB_Credentials
import sys
import csv
import psycopg2
import datetime

sys.path.append('..')
from Credentials import DB_Credentials

import os

def main(create_zip = False, password = 'ZIP_PASS' in os.environ and os.environ['ZIP_PASS']):
    siraAdminName = DB_Credentials.Siralogin['username']
    siraAdminPassword = DB_Credentials.Siralogin['password']
    siraDbUrl = DB_Credentials.Siralogin['url']
    try:
        connSIRA = psycopg2.connect(database="sira", user=siraAdminName, password=siraAdminPassword, host=siraDbUrl)
        print ("Connected to Sira database\n")
    except:
        print ("Can't connect to SIRA database\n")
        return 1

    siraCur = connSIRA.cursor()
    total = 0
    driving = 0
    filedate = str(datetime.datetime.now()).split(' ')[0]
    filename = 'gofar_sira_' + filedate + '.csv'
    with open(filename, 'wb') as csvfile:
        fieldnames = ['e-mail', 'Firstname', 'Lastname','Make', 'Model','TripCount', 'TotalDist[km]','FirstTripDate', 'LastTripDate', 'FirsTripDate', 'DriverScore','AccelerationScore','BrakingScore', 'Group' ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect='excel')
        writer.writeheader()
        userQuery = """
                select "user".email, "user".firstname, "user".lastname, vehicle.make, vehicle.model
                    , "user".bondedvehicleid -- r[5]
                    , "user".id from "user"
                    left join vehicle on vehicle.id = "user".bondedvehicleid
                    where "user".email NOT like '%gofar.co' and "user".email NOT like '%.pl'
                """
        siraCur.execute(userQuery)
        userResult = siraCur.fetchall()
        for r in userResult:
            total +=1
            vehicleResult = ['0','0','unknown', 'unknown']
            vMake = 'unknown'
            vModel = 'unknown'
            if r[5]:
                driving += 1
                tripQuery = """
                    select count(*) as num_trips, sum(trip.distance) as total_kms
                        , date_trunc('day', max(trip.starttime)) as last_trip_date
                        , date_trunc('day', min(trip.starttime)) as first_trip_date
                        from trip inner join vehicle on trip.vehicleid = vehicle.id
                        where vehicle.id = %(vid)s
                        and trip.starttime > '2018-01-01'
                    """
                print 'tripquery', tripQuery, r[5]
                siraCur.execute(tripQuery,{"vid":r[5]})
                vehicleResult = siraCur.fetchall()[0]
                if vehicleResult[0] == 0:
                    vehicleResult = ['0','0','unknown','unknown']
            if r[3]:
                vMake = r[3]
            if r[4]:
                vModel = r[4]
            summaryQuery = """
                select score, brakingscore, leaderboard."accelerationScore" from leaderboard where leaderboard.userid = %(uid)s
            """
            siraCur.execute(summaryQuery,{"uid":r[6]})
            summaryResult = siraCur.fetchall()[0]
            if summaryResult[0] is None:
                summaryResult = [0,0,0]
            siraCur.execute("""
                select subscriptiontypeid from subscription where subscription.userid= %(uid)s
                """,
                {"uid":r[6]})
            subsResult = siraCur.fetchall()[0]
            subTypes = {'SIRA_USER_CONTROL' : 'Control', 'SIRA_USER_DEFAULT':'Treatment'}
            sub = 'unknown'
            try:
                sub = subTypes[subsResult[0]]
            except:
                print 'Unknown subscription for user : ' + r[0]
            try :
                writer.writerow({'e-mail':r[0], 
                    'Firstname': r[1], 
                    'Lastname': r[2], 
                    'Make': vMake,
                    'Model':vModel,
                    'TripCount': vehicleResult[0], 
                    'TotalDist[km]':round(float(vehicleResult[1] if vehicleResult[1] else 0),2),
                    'FirstTripDate': (str(vehicleResult[3]).split(' '))[0],
                    'LastTripDate': (str(vehicleResult[2]).split(' '))[0], 
                    'DriverScore' : round(summaryResult[0], 3), 
                    'AccelerationScore': round(summaryResult[2], 3), 
                    'BrakingScore': round(summaryResult[1], 3),
                    'Group':sub
                })
            except Exception, e:
                print 'exception : '
                print str(e)
                print vehicleResult
                print r
                print tripQuery
    connSIRA.close()
    print 'Sira summary for ' + filedate + ' : '
    print 'Total Users : ' + str(total)
    print 'Driving Users : ' + str(driving)
    print 'Results saved to : ', filename
    if create_zip:
        import os
        zipfile = "%s.zip" % filename
        os.system('sed -i "s/_turnedoff//" %s; zip --password "%s" "%s" "%s"' % (
                filename,
                password,
                zipfile,
                filename
            )
        )
        return zipfile
    return filename
    
if __name__ == '__main__':
    main()
    pass
